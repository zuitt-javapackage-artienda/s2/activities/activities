package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String[]args){

        int[] intArray = {1, 2, 3, 4, 5};
        System.out.println("The first prime number is : " + intArray[0]);

        ArrayList<String> myFriends = new ArrayList<>(Arrays.asList("Kim", "Lester", "Chit", "Jager"));
        System.out.println("My friends are: " + myFriends);

        HashMap<String, Integer> myComputer = new HashMap<>();
        myComputer.put("Mouse", 2000);
        myComputer.put("Monitor", 15000);
        myComputer.put("Keyboard", 500);
        myComputer.put("CPU", 45000);
        System.out.println("My computer set-up cost: " + myComputer);




    }
}
